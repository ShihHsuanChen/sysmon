# sudo sh run.sh
PGID=$(grep docker /etc/group | cut -d ':' -f 3)
sed -i -E 's/PGID=(.+){0,1}$/PGID='${PGID}'/' docker-compose.yml
docker-compose up -d
docker ps -a
