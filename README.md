# Sysmon

System monitor in docker container. Docker image netdata/netedata is used.

### System requirements
- Ubuntu 18.04-LTS
- docker 19.03
    ```sh
    $ docker version
    Client: Docker Engine - Community
     Version:           19.03.7
     API version:       1.40
     Go version:        go1.12.17
     Git commit:        7141c199a2
     Built:             Wed Mar  4 01:22:36 2020
     OS/Arch:           linux/amd64
     Experimental:      false

    Server: Docker Engine - Community
     Engine:
      Version:          19.03.7
      API version:      1.40 (minimum version 1.12)
      Go version:       go1.12.17
      Git commit:       7141c199a2
      Built:            Wed Mar  4 01:21:08 2020
      OS/Arch:          linux/amd64
      Experimental:     false
     containerd:
      Version:          1.2.13
      GitCommit:        7ad184331fa3e55e52b890ea95e65ba581ae3429
     runc:
      Version:          1.0.0-rc10
      GitCommit:        dc9208a3303feef5b3839f4323d9beb36df0a9dd
     docker-init:
      Version:          0.18.0
      GitCommit:        fec3683
    ```
- docker-compose
    ```sh 
    $ docker-compose version
    docker-compose version 1.24.0, build 0aa59064
    docker-py version: 3.7.2
    CPython version: 3.6.8
    OpenSSL version: OpenSSL 1.1.0j  20 Nov 2018
    ```

## Run

1. Go to the server
2. Git clone from [gitlab](http://gitlab.sltung.com.tw/medical_image_ai/common/sysmon)
    ```sh
    $ git clone http://gitlab.sltung.com.tw/medical_image_ai/common/sysmon.git
    ```
    or copy ``docker-compose.yml`` and ``run.sh`` to the target directory.
3. Change the permission of run.sh file to executable.
    ```sh 
    $ chmod a+x run.sh
    ```
4. Execute run.sh
    ```sh 
    $ ./run.sh
    ```
5. Check status
    ```sh
    $ docker-compose ps
    CONTAINER ID        IMAGE              COMMAND              CREATED   STATUS    PORTS     NAMES
    ...                 netdata/netdata    "/usr/sbin/run.sh"   ...       up ...              sysmon_netdata_1
    ...
    ```
6.  Open your browser and check the website of server IP and the export port.
